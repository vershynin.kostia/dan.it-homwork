import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class homework1 {
//Vershynin Kostintun
    public static void main(String[] args) {

        Random rnd = new Random();
        int number = rnd.nextInt(100) + 1;
        System.out.println("Let the game begin!");
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter your name: ");
        String name = scan.nextLine();
        int count = 0;
        int[] arr = new int[100];

        while (true) {

            System.out.println("Enter your number: ");

            if (scan.hasNextInt()) {
                int numInput = scan.nextInt();
                arr[count] = numInput;
                count++;
                if (number > numInput) {
                    System.out.println("Your number is too small. Please, try again");
                }
                if (number < numInput) {
                    System.out.println("Your number is too big. Please, try again");
                }
                if (number == numInput) {
                    System.out.println("Congratulations " + name + "!!!");
                    break;
                }
            }else {
                System.out.println("You entered the wrong number");

            }
                scan.nextLine();

        }
        scan.close();

        int[] result = new int[count];

        for (int i = 0; i < count; i++) {
            result[i] = arr[i];
        }

        bubbleSort(result);
        System.out.println("Your numbers: ");
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }

    }


    private static void bubbleSort(int[] intArray) {


        int n = intArray.length;
        int temp = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {

                if (intArray[j - 1] > intArray[j]) {
                    //swap the elements!
                    temp = intArray[j - 1];
                    intArray[j - 1] = intArray[j];
                    intArray[j] = temp;
                }

            }
        }
    }
}
